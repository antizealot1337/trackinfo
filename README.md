# Trackinfo
This is a command line tool to manipulate and display ID3v2 data.

## Usage
The basic usage is available from the command line.

```text
Description: Tool to display and manipulate id3v2 values for audio files.

Usage:  trackinfo [options] [AUDIO_FILES...]

Flags:
  -A, --album string    The albumn
  -a, --artist string   The artist
  -d, --display         Display track info instead of updating
  -g, --genre string    The genre
  -i, --index int       The index of the track
  -n, --title string    The title
  -t, --total int       The total number of tracks
  -v, --verbose         Output information
  -V, --version         Show the version
  -y, --year int        The year
```

## Examples
Below are some usage examples.

Updating the information for a file is as simple as providing the information
(via CLI flag) and the file to update.

`trackinfo --title 'Some title' file.mp3`

Multiple values can be updated and multiple files can be updated as well.

`trackinfo --album 'Album Name' --total 2 file1.mp3 file2.mp3`

Providing the `--verbose` flag will cause the program will be more informative
about what it's doing.

```text
trackinfo --title 'Some title' file.mp3
Updating title: SomeTitle
```

View the information of the file is done with the `--display` CLI flag.

`trackinfo --display file.mp3`

To view raw ID3v2 tags add the `--verbose` CLI flag.

`trackinfo --display --verbose file.mp3`

Like updating multiple files may be viewed at the same time.

`trackinfo --display file1.mp3 file2.mp3`

## Copyright
Freely available under the terms of the MIT License. Please view license file
for more information.
