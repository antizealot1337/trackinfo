module gitlab.com/antizealot1337/trackinfo

go 1.17

require (
	bitbucket.org/antizealot1337/pflag v0.0.0-20210629152809-98ed1699e865
	github.com/bogem/id3v2/v2 v2.1.4
)

require golang.org/x/text v0.8.0 // indirect
