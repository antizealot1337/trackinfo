# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.3] - 2023-03-30
### Changed
- Upgraded dependencies

## [0.2.2] - 2023-01-05
### Changed
- Upgraded dependencies

## [0.2.0] - 2022-07-04
### Changed
- Change package to gitlab.com/antizealot1337/trackinfo

## [0.1.0] - 2021-09-02
### Added
- Added ability to modify album for audio file(s)
- Added ability to modify artist for audio file(s)
- Added ability to modify genre for audio file(s)
- Added ability to modify index for audio file(s)
- Added ability to modify title for audio file(s)
- Added ability to modify total for audio file(s)
- Added ability to modify release year for audio file(s)
- Added ability to display info for audio file(s)
