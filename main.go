package main

import (
	"fmt"
	"os"
	"strconv"

	flag "bitbucket.org/antizealot1337/pflag"
	"github.com/bogem/id3v2/v2"
)

const Version = "0.2.3"

const (
	APIC       = "APIC"
	TRCK       = "TRCK"
	TXXX       = "TXXX"
	TrackTotal = "TRACKTOTAL"
)

func main() {
	var (
		title       string
		album       string
		artist      string
		genre       string
		index       int
		total       int
		year        int
		display     bool
		verbose     bool
		showVersion bool
	)

	var err error

	fs := flag.NewFlagSet(os.Args[0], flag.ExitOnError)

	fs.StringVarP(&title, "title", "n", "", "The title")
	fs.StringVarP(&artist, "artist", "a", "", "The artist")
	fs.StringVarP(&album, "album", "A", "", "The albumn")
	fs.StringVarP(&genre, "genre", "g", "", "The genre")
	fs.IntVarP(&index, "index", "i", 0, "The index of the track")
	fs.IntVarP(&total, "total", "t", 0, "The total number of tracks")
	fs.IntVarP(&year, "year", "y", 0, "The year")
	fs.BoolVarP(&display, "display", "d", false, "Display track info instead of updating")
	fs.BoolVarP(&verbose, "verbose", "v", verbose, "Output information")
	fs.BoolVarP(&showVersion, "version", "V", showVersion, "Show the version")

	fs.Usage = func() {
		out := fs.Output()

		cmd := os.Args[0]

		fmt.Fprintln(out, "Description: Tool to display and manipulate id3v2 values for audio files.")
		fmt.Fprintln(out)
		fmt.Fprintln(out, "Usage: ", cmd, "[options] [AUDIO_FILES...]")
		fmt.Fprintln(out)
		fmt.Fprintln(out, "Flags:")
		fs.PrintDefaults()
	} //func

	err = fs.Parse(os.Args[1:])
	check(err)

	if showVersion {
		fmt.Println(Version)
		return
	} //if

	for idx, arg := range fs.Args() {
		if verbose {
			if display {
				fmt.Println("Updating", arg, "with album info")
			} else {
				fmt.Println("Displaying", arg)
			} //if
		} //if
		if display {
			err = displayInfo(arg, verbose)
			if err == nil && idx+1 < len(fs.Args()) {
				fmt.Println()
			} //if
		} else {
			err = updateInfo(arg, title, album, artist, genre, index, total, year, verbose)
		} //if

		check(err)
	} //for
} //func

func updateInfo(filename string, title, album, artist, genre string, num, total, year int, verbose bool) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("update info: %w", err)
		} //if
	}() //func

	tag, err := id3v2.Open(filename, id3v2.Options{Parse: true})
	if err != nil {
		return
	} //if

	if title != "" {
		if verbose {
			fmt.Println("Updating title:", title)
		} //if
		tag.SetTitle(title)
	} //if
	if album != "" {
		if verbose {
			fmt.Println("Updating album:", album)
		} //if

		tag.SetAlbum(album)
	} //if
	if artist != "" {
		if verbose {
			fmt.Println("Updating artist:", artist)
		} //if

		tag.SetArtist(artist)
	} //if
	if genre != "" {
		if verbose {
			fmt.Println("Updating genre:", genre)
		} //if

		tag.SetGenre(genre)
	} //if

	if year != 0 {
		if verbose {
			fmt.Println("Updating year:", year)
		} //if

		tag.SetYear(strconv.Itoa(year))
	} //if
	if num != 0 {
		if verbose {
			fmt.Println("Updating index:", num)
		} //if

		tag.AddTextFrame(TRCK, id3v2.EncodingISO, strconv.Itoa(num))
	} //if
	if total != 0 {
		if verbose {
			fmt.Println("Updating total:", total)
		} //if

		tag.AddFrame(TXXX, id3v2.UserDefinedTextFrame{
			Encoding:    id3v2.EncodingISO,
			Description: TrackTotal,
			Value:       strconv.Itoa(total),
		})
	} //if

	err = tag.Save()
	return
} //func

func check(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		os.Exit(1)
	} //if
} //func

func displayInfo(filename string, verbose bool) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("display info: %w", err)
		} //if
	}() //func

	tag, err := id3v2.Open(filename, id3v2.Options{Parse: true})
	if err != nil {
		return
	} //if

	if verbose {
		out := os.Stdout
		for key, framers := range tag.AllFrames() {
			showContents := key != APIC
			fmt.Fprintln(out, key)
			for _, framer := range framers {
				fmt.Fprintf(out, "  %s(size:%d)", framer.UniqueIdentifier(), framer.Size())
				if showContents {
					framer.WriteTo(out)
				} //if
				fmt.Fprintln(out)
			} //for
		} //for
	} else {
		fmt.Println("Title:   ", tag.Title())
		fmt.Println("Albumn:  ", tag.Album())
		fmt.Println("Artist:  ", tag.Artist())
		fmt.Println("Genre:   ", tag.Genre())
		fmt.Println("Year:    ", tag.Year())
		index, total := getIndex(tag), getTotal(tag)
		fmt.Printf("Position: %d/%d\n", index, total)
	} //if

	return
} //func

func getIndex(tag *id3v2.Tag) (index int) {
	index, _ = strconv.Atoi(tag.GetTextFrame(TRCK).Text)
	return
} //func

func getTotal(tag *id3v2.Tag) (total int) {
	framers := tag.GetFrames(TXXX)
	for _, framer := range framers {
		if framer.UniqueIdentifier() == TrackTotal {
			udtf := framer.(id3v2.UserDefinedTextFrame)
			total, _ = strconv.Atoi(udtf.Value)
		} //if
	} //for

	return
} //func
